import os
import h5py
from time import sleep
import multiprocessing


def writer1(filename, **kw):
    os.environ["HDF5_USE_FILE_LOCKING"] = "TRUE"
    with h5py.File(filename, **kw) as f:
        f["check"] = True
        f.flush()
        print("writer1 holds file open")
        sleep(1000)


def writer2(filename, **kw):
    os.environ["HDF5_USE_FILE_LOCKING"] = "TRUE"
    try:
        with h5py.File(filename, **kw) as f:
            pass
    except Exception as e:
        print("writer2 failed as expected")
        pass  # locked by writer1


def reader(filename, **kw):
    os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
    with h5py.File(filename, mode="r", **kw) as f:
        assert f["check"][()]
    print("HDF5 valid")


def main(lock_issue=False, wmode_issue=False):
    print(h5py.version.version)
    print(h5py.version.hdf5_version)
    assert os.environ.get("HDF5_USE_FILE_LOCKING") is None

    filename = "test.h5"
    try:
        os.unlink(filename)
    except FileNotFoundError:
        pass

    if lock_issue:
        # This does not allow a HDF5_USE_FILE_LOCKING=FALSE reader
        libver = "latest"
    else:
        libver = None

    p = multiprocessing.Process(
        target=writer1, args=(filename,), kwargs={"mode": "a", "libver": libver}
    )
    p.start()
    sleep(1)
    try:
        reader(filename)
        if wmode_issue:
            # This corrupts the file, despite the lock!!!
            writer2_mode = "w"
        else:
            writer2_mode = "a"
        writer2(filename, mode=writer2_mode, libver=libver)
        reader(filename, libver=libver)
    finally:
        p.kill()


main(lock_issue=False, wmode_issue=False)
