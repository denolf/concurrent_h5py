"""
DEMO_SESSION [1]: while True: loopscan(200, 0.1)
"""

import os
import math
import logging
import gevent

import silx.io.h5py_utils

from bliss.data.node import get_node, get_session_node
from nexus_writer_service.utils.scan_utils import scan_uris


logger = logging.getLogger(__file__)


EVENT_FILTER = ("scan", "scan_group")


@silx.io.h5py_utils.retry(retry_timeout=5)
def get_hdf5_current_point(file, path):
    with silx.io.h5py_utils.File(file) as h5:
        group = h5[path]
        min_points = math.inf
        names = list(group.keys())
        for dataset in names:
            try:
                d = group[dataset]
            except KeyError:
                continue  # link destination does not exist yet
            point = d.shape[0]
            if point < min_points:
                min_points = point
        return min_points


@silx.io.h5py_utils.retry(retry_timeout=5)
def get_last_scan(file, path, log):
    with silx.io.h5py_utils.File(file) as h5:
        base = h5["1.1"]
        root = h5["1.1/measurement"]

        scan_numbers = None
        if "scan_numbers" in root:
            scan_numbers = root["scan_numbers"][()]

        if base["title"][()] == b"sequence_of_scans" and scan_numbers is not None:
            if len(scan_numbers) > 0:
                new_path = f"{scan_numbers[-1]}.1/measurement"
                log.info(
                    f"File is sequence of scans, changing path to latest scan: {new_path}"
                )
                return new_path
            else:
                log.info("Scan numbers has zero length for %s", file)

        return path


def test_uri(uri, stop):
    file, path = uri.split("::")
    path += "/measurement"

    nsuccesses = 0
    nfailures = 0
    while not stop.is_set():
        # try:
        #    get_last_scan(file, path, logger)
        # except Exception as e:
        #    logger.exception(e)
        #    nfailures += 1
        # else:
        #    nsuccesses += 1

        try:
            get_hdf5_current_point(file, path)
        except Exception as e:
            e = e.__cause__
            # logger.exception(e)
            print("Failure", type(e), e)
            nfailures += 1
        else:
            nsuccesses += 1
        gevent.sleep(0.1)
    return f"nsuccesses: {nsuccesses}, nfailures: {nfailures}"


def scan_watcher(scan_name):
    """Monitor scan events
    """
    scan = get_node(scan_name)
    uris = scan_uris(scan)  # HDF5 scan entrees of the scan
    print(f"\nSTART {uris} ...")
    stop = gevent.event.Event()
    glts = [gevent.spawn(test_uri, uri, stop) for uri in uris]

    # Wait until the scan is finished
    for ev in scan.walk_events(
        include_filter=EVENT_FILTER, exclude_children=EVENT_FILTER
    ):
        if ev.type == ev.type.END_SCAN:
            break

    # Stop testing the file and print the result
    stop.set()
    for uri, g in zip(uris, glts):
        print(uri, g.get())
    print(f"END {uris}.")


def session_watcher(session_name, new=True):
    """Add scan watchers to new scans
    """
    print("CTRL-C to stop listening to the session")
    node = get_session_node(session_name)
    if new:
        gen = node.walk_on_new_events
    else:
        gen = node.walk_events
    glts = dict()
    try:
        for ev in gen(include_filter=EVENT_FILTER, exclude_children=EVENT_FILTER):
            if ev.type == ev.type.NEW_NODE:
                g = gevent.spawn(scan_watcher, ev.node.db_name)
                glts[ev.node.db_name] = g
            elif ev.type == ev.type.END_SCAN:
                glts.pop(ev.node.db_name)
    finally:
        for g in glts.values():
            g.kill()


if __name__ == "__main__":
    os.environ["TANGO_HOST"] = "localhost:10000"
    os.environ["BEACON_HOST"] = "localhost:10001"
    session_watcher("demo_session", new=True)
