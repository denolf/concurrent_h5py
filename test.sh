#!/bin/bash
#
# This should never crash:
#
#  ./test.sh
#

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

spin() {
   local -a marks=( '/' '-' '\' '|' )
   while [[ 1 ]]; do
     printf '%s\r' "${marks[i++ % ${#marks[@]}]}"
     sleep 1
   done
 }


function main(){
    local reader_args=""

    while getopts "htp" opt; do
      case $opt in
        t)
          reader_args="--usescanfile"
          ;;
        p)
          reader_args="--partialretry"
          ;;
        h)
          echo "Usage:" >&2
          echo " ./test.sh [-t] [-f]" >&2
          exit 0
          ;;
        \?)
          echo "Invalid option: -$OPTARG" >&2
          exit 1
          ;;
        :)
          echo "Option -$OPTARG requires an argument." >&2
          exit 1
          ;;
      esac
    done

    rm -f *.log *.h5

    sleep 1

    # get the core
    ulimit -c 100000000

    # Single writer
    python3 $SCRIPT_ROOT/test_concurrent_rw.py --writer --n 5000 2>&1 &

    # Multiple readers
    for i in {1..12}; do
        python3 $SCRIPT_ROOT/test_concurrent_rw.py $reader_args 2>&1 &
    done
    #spin &

    trap 'echo "Test interrupted: killing subprocessed ...";kill $(jobs -p)' INT
    
    wait -n
    echo "At least one subprocess stopped, killing all subprocessed ..."
    kill $(jobs -p)
}

main $@
