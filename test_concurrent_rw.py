"""Test processing an HDF5 file that is being written

Launch the writer (only one):

    python test_concurrent_rw.py --writer

Launch the readers (as many as you want):

    python test_concurrent_rw.py
"""

import os
import sys
import time
import datetime
import signal
import numpy
import logging
from contextlib import contextmanager

from silx.io import h5py_utils


logging.basicConfig(
    level=logging.INFO, format=f"%(asctime)s - [pid={os.getpid()}] %(message)s"
)


logger = logging.getLogger()


# Global writer/reader configuration
h5py_utils.retry_mod.RETRY_PERIOD = 0.001
FLUSH_PERIOD = 0.5
NCOL = 10
NROW = 5
NDATASET = 3
TEST_DATA = numpy.arange(0, NROW * NCOL).reshape(NROW, NCOL)


def process_scan(scan):
    if scan is None or "end_time" not in scan:
        logger.debug("Scan not processed (not complete yet)")
        return False
    scan_name = scan.name
    logger.debug("processing scan " + scan_name + "...")
    measurement = scan["measurement"]
    for k in measurement:
        dset = measurement[k]
        data = dset[()]
        numpy.testing.assert_array_equal(data, TEST_DATA)
    logger.debug("scan " + scan_name + " processed")
    return True


@h5py_utils.retry()
def process_scan_full_retry(filename, scan_name):
    """
    Retry until "open", "get scan group", "process data"
    """
    with h5py_utils.File(filename) as h5file:
        return process_scan(h5file[scan_name])


def process_scan_partial_retry(filename, scan_name):
    """
    Retry until "open", "get scan group".
    process_scan sometimes raises an exception which requires re-opening
    the file, despite processing a finished scan.
    """
    with h5py_utils.open_item(
        filename, scan_name, validate=h5py_utils.group_has_end_time
    ) as scan:
        return process_scan(scan)

    #  File "test_concurrent_rw.py", line 50, in process_scan
    #    data = dset[()]
    #  File "h5py/_objects.pyx", line 54, in h5py._objects.with_phil.wrapper
    #  File "h5py/_objects.pyx", line 55, in h5py._objects.with_phil.wrapper
    #  File "/users/denolf/virtualenvs/h5py/ubuntu_20_04/py38/lib/python3.8/site-packages/h5py/_hl/dataset.py", line 787, in __getitem__
    #    self.id.read(mspace, fspace, arr, mtype, dxpl=self._dxpl)
    #  File "h5py/_objects.pyx", line 54, in h5py._objects.with_phil.wrapper
    #  File "h5py/_objects.pyx", line 55, in h5py._objects.with_phil.wrapper
    #  File "h5py/h5d.pyx", line 192, in h5py.h5d.DatasetID.read
    #  File "h5py/_proxy.pyx", line 112, in h5py._proxy.dset_rw
    # OSError: Can't read data (addr overflow, addr = 1909084, size = 2616, eoa = 1895158)


def process_scan_swmr(filename, scan_name):
    """
    Retry until "open", "get scan group", "process data"
    """
    with h5py_utils.File(filename, swmr=True) as h5file:
        return process_scan(h5file[scan_name])


def reader_processed():
    processed = []

    def signal_handler(sig, frame):
        sys.exit(0)

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGHUP, signal_handler)
    return processed


def get_scan_names(filename):
    return h5py_utils.safe_top_level_names(filename)


def get_scan_names_from_scan_file(filename):
    sname = filename + "_scans.log"
    if os.path.exists(sname):
        return [s.rstrip() for s in open(sname, "r").readlines()]
    else:
        return []


@h5py_utils.retry_contextmanager()
def open_swmr(filename):
    with h5py_utils.File(filename, swmr=True) as h5file:
        yield h5file


def get_scan_names_swmr(filename):
    with open_swmr(filename) as h5file:
        return [
            name for name in h5file["/"] if h5py_utils.group_has_end_time(h5file[name])
        ]


def reader(filename, from_scan_file=False, partial_retry=False, swmr=False):
    """Main reader loop"""
    try:
        processed = reader_processed()
        if swmr:
            func = process_scan_swmr
        elif partial_retry:
            func = process_scan_partial_retry
        else:
            func = process_scan_full_retry
        while True:
            if from_scan_file:
                scan_names = get_scan_names_from_scan_file(filename)
            elif swmr:
                scan_names = get_scan_names_swmr(filename)
            else:
                scan_names = get_scan_names(filename)
            nbefore = len(processed)
            for scan_name in scan_names:
                if scan_name in processed:
                    continue
                try:
                    if func(filename, scan_name):
                        processed.append(scan_name)
                except Exception:
                    logger.error("ERROR processing in " + scan_name)
                    raise
            nafter = len(processed)
            if nafter > nbefore:
                logger.info(str(nafter) + " scans processed")
    except Exception as e:
        logger.error("ERROR " + repr(e))
        raise
    finally:
        logger.warning(
            "End reader process: " + str(len(processed)) + " scans processed"
        )


def write_scan(f, scan_name, resize=True, compression="gzip"):
    logger.debug("writing scan " + scan_name)
    scan = f.create_group(scan_name)
    scan.attrs["NX_class"] = "NXentry"
    scan["start_time"] = datetime.datetime.now().isoformat()
    measurement = scan.create_group("measurement")
    measurement.attrs["NX_class"] = "NXcollection"

    t0 = time.time()

    if resize:
        shape = (1, TEST_DATA.shape[1])
        maxshape = (None, TEST_DATA.shape[1])
    else:
        shape = TEST_DATA.shape
        maxshape = None

    datasets = []
    for i in range(NDATASET):
        dset = measurement.create_dataset(
            "data" + str(i), shape=shape, maxshape=maxshape, compression=compression
        )
        datasets.append(dset)
        dset[0] = TEST_DATA[0]
        if time.time() - t0 > FLUSH_PERIOD:
            f.flush()
            t0 = time.time()
            logger.debug("flush")
    f.flush()
    logger.debug("datasets created")

    for dset in datasets:
        for j in range(1, TEST_DATA.shape[0]):
            if resize:
                dset.resize((j + 1, TEST_DATA.shape[1]))
            dset[j] = TEST_DATA[j]
        if time.time() - t0 > FLUSH_PERIOD:
            f.flush()
            t0 = time.time()
            logger.debug("writer flush")
    logger.debug(scan_name + " data saved")

    scan["end_time"] = datetime.datetime.now().isoformat()
    f.flush()
    logger.info("finished writing scan " + scan_name)


def writer(filename, n=20, swmr=False, close_after_each_scan=False):
    """Main writer loop"""
    try:
        os.unlink(filename)
    except FileNotFoundError:
        pass
    if close_after_each_scan:
        n1, n2 = n, 1
    else:
        n1, n2 = 1, n
    for _ in range(n1):
        # libver=None: exceptions in reader
        # libver="v110": no exceptions in reader but reader can only get hold
        #                of the file when writer closed it
        logger.info("Writer opening file ...")
        with h5py_utils.File(filename, mode="a", swmr=swmr) as f:
            logger.info("Writer opened file")
            for _ in range(n2):
                scans = list(f["/"])
                if scans:
                    scan_number = int(scans[-1].split(".")[0]) + 1
                else:
                    f.attrs["NX_class"] = "NXroot"
                    scan_number = 1
                scan_name = str(scan_number) + ".1"
                write_scan(f, scan_name)
                with open(filename + "_scans.log", "a") as scanfile:
                    scanfile.write("%s\n" % (scan_name))
            logger.debug("Writer closing file ...")
        logger.info("Writer closed file")

    logger.info("End writer process")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Test processing an HDF5 which is being written."
    )
    parser.add_argument(
        "--writer", action="store_true", help="Run a writer (do only once)"
    )
    parser.add_argument("--swmr", action="store_true", help="SWMR mode")
    parser.add_argument(
        "--closeafterscan",
        action="store_true",
        help="Writer closes the file after each scan",
    )
    parser.add_argument(
        "--usescanfile", action="store_true", help="Use the scan file for reading"
    )
    parser.add_argument("--n", type=int, default=20, help="Number of scans to write")
    parser.add_argument(
        "--partialretry", action="store_true", help="Retry scan processing partially"
    )
    args = parser.parse_args()
    if args.writer:
        writer(
            "test.h5",
            n=args.n,
            swmr=args.swmr,
            close_after_each_scan=args.closeafterscan,
        )
    else:
        reader(
            "test.h5",
            from_scan_file=args.usescanfile,
            partial_retry=args.partialretry,
            swmr=args.swmr,
        )
