# concurrent_h5py

Test project to investigate concurrent reading and writing with h5py.

Based on https://gitlab.esrf.fr/wright/rwh5bug/

## Test on a single machine

Test concurrent reading and writing one the same machine:

```bash
./test.sh
```

The output will look like this:

```bash
2021-11-17 14:16:53,997 - [pid=548440] 3436 scans processed  <-- reader progress
2021-11-17 14:16:54,002 - [pid=548434] finished writing scan 3477.1  <-- writer progress
2021-11-17 14:16:54,015 - [pid=548434] finished writing scan 3478.1
2021-11-17 14:16:54,028 - [pid=548434] finished writing scan 3479.1
2021-11-17 14:16:54,031 - [pid=548444] 3429 scans processed
2021-11-17 14:16:54,041 - [pid=548434] finished writing scan 3480.1
2021-11-17 14:16:54,054 - [pid=548434] finished writing scan 3481.1
2021-11-17 14:16:54,068 - [pid=548434] finished writing scan 3482.1
2021-11-17 14:16:54,094 - [pid=548434] finished writing scan 3483.1
2021-11-17 14:16:54,107 - [pid=548434] finished writing scan 3484.1
2021-11-17 14:16:54,116 - [pid=548443] 3439 scans processed
2021-11-17 14:16:54,131 - [pid=548434] finished writing scan 3485.1
2021-11-17 14:16:54,136 - [pid=548445] 3447 scans processed
2021-11-17 14:16:54,147 - [pid=548434] finished writing scan 3486.1
2021-11-17 14:16:54,160 - [pid=548434] finished writing scan 3487.1
2021-11-17 14:16:54,192 - [pid=548434] finished writing scan 3488.1
```

The test is successfull when:

 1. none of the processes end with a SEGFAULT
 2. the reader progress is not stuck (it keeps up more or less with the writer)

## Test on different machines

Alternatively start a writer on one machine

```bash
python test_concurrent_rw.py --writer --n 5000
```

and a reader on another machine

```bash
python test_concurrent_rw.py
```
